/**
 * Precompiled [com.gitee.lz.libs.gradle.kts][Com_gitee_lz_libs_gradle] script plugin.
 *
 * @see Com_gitee_lz_libs_gradle
 */
class Com_gitee_lz_libsPlugin : org.gradle.api.Plugin<org.gradle.api.Project> {
    override fun apply(target: org.gradle.api.Project) {
        try {
            Class
                .forName("Com_gitee_lz_libs_gradle")
                .getDeclaredConstructor(org.gradle.api.Project::class.java, org.gradle.api.Project::class.java)
                .newInstance(target, target)
        } catch (e: java.lang.reflect.InvocationTargetException) {
            throw e.targetException
        }
    }
}

import com.gitee.lz.ExternalLibrariesExtension

plugins {
    checkstyle
    `java-library`
    `maven-publish`
}

val projectVersionApiHubService: String by project

allprojects {
    group = "com.gitee.lz"
    version = projectVersionApiHubService
}

repositories {

    maven {
        url = uri("https://maven.aliyun.com/nexus/content/groups/public")
    }

    maven {
        url = uri("https://repo.maven.apache.org/maven2/")
    }

    mavenLocal()
}

java.sourceCompatibility = JavaVersion.VERSION_1_8
java.targetCompatibility = JavaVersion.VERSION_1_8

checkstyle {
    toolVersion = "8.29"
    isIgnoreFailures = false
    maxWarnings = 0
    configFile = file("$rootDir/checkstyle.xml")
}

tasks.checkstyleTest {
    isEnabled = false
}

tasks.checkstyleMain {
    source = fileTree("src/main/java")
}

// file encoding
tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
}

tasks.withType<Test> {
    systemProperty("file.encoding", "UTF-8")
}

tasks.withType<Javadoc>{
    options.encoding = "UTF-8"
}

publishing {
    publications.create<MavenPublication>("mavenJava") {
        from(components["java"])
    }
    repositories {
        mavenLocal()
    }
}

tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
}

tasks.withType<Javadoc> {
    options.encoding = "UTF-8"
}

tasks.withType<Test> {
    defaultCharacterEncoding = "UTF-8"
}

extensions.create<com.gitee.lz.ExternalLibrariesExtension>("libs")

package com.gitee.lz

@Suppress("unused")
abstract class ExternalLibrariesExtension {
    object Group {
        val protobuf = "com.google.protobuf"
        val grpc = "io.grpc"
        val springBoot = "org.springframework.boot"
        val devhGrpc = "net.devh"
        val apacheCommons = "org.apache.commons"
        val mabatisPlus = "com.baomidou"
        val flyway = "org.flywaydb"
    }

    object Version {
        val protobuf = "3.21.1"
        val grpc = "1.42.2"
        val springBoot = "2.7.0"
        val devhGrpc = "2.13.1.RELEASE"
        val mybatisPlus = "3.5.4"
        val flyway = "8.5.12"
        var vertxVersion = "4.3.8"
        var jacksonVersion = "2.15.0"
        val akkaVersion = "2.8.5"
    }

    val protobufJavaCore = "${Group.protobuf}:protobuf-java:${Version.protobuf}"
    val protobufJavaUtil = "${Group.protobuf}:protobuf-java-util:${Version.protobuf}"
    val protoc = "${Group.protobuf}:protoc:${Version.protobuf}"

    val snakeyaml = "org.yaml:snakeyaml:1.30"

    val jacksonProtobuf = "com.hubspot.jackson:jackson-datatype-protobuf:0.9.12"

    val grpcProtobuf = "${Group.grpc}:grpc-protobuf:${Version.grpc}"
    val grpcStub = "${Group.grpc}:grpc-stub:${Version.grpc}"
    val grpcProtocGenJava = "${Group.grpc}:protoc-gen-grpc-java:${Version.grpc}"
    val grpcTesting = "${Group.grpc}:grpc-testing:${Version.grpc}"

    val javaxAnnotation = "javax.annotation:javax.annotation-api:1.3.2"

    val springBootStarterWeb = "${Group.springBoot}:spring-boot-starter-web:${Version.springBoot}"
    val springBootStarterDataRedis = "${Group.springBoot}:spring-boot-starter-data-redis:${Version.springBoot}"
    val springBootStarterDataJdbc = "${Group.springBoot}:spring-boot-starter-data-jdbc:${Version.springBoot}"
    val springBootStarterTest = "${Group.springBoot}:spring-boot-starter-test:${Version.springBoot}"
    val springBootConfigProcessor = "${Group.springBoot}:spring-boot-configuration-processor:${Version.springBoot}"
    val springActuatorAutoConfig = "${Group.springBoot}:spring-boot-actuator-autoconfigure:${Version.springBoot}"

    val devhGrpcClientSpringBoot = "${Group.devhGrpc}:grpc-client-spring-boot-starter:${Version.devhGrpc}"
    val devhGrpcServerSpringBoot = "${Group.devhGrpc}:grpc-server-spring-boot-starter:${Version.devhGrpc}"

    val logback = "ch.qos.logback:logback-classic:1.2.11"

    val flywayCore = "${Group.flyway}:flyway-core:${Version.flyway}"
    val flywayMysql = "${Group.flyway}:flyway-mysql:${Version.flyway}"

    val micrometerRegistryPrometheus = "io.micrometer:micrometer-registry-prometheus:1.9.0"

    val myBatisPlusBootStarter = "${Group.mabatisPlus}:mybatis-plus-boot-starter:${Version.mybatisPlus}"
    val myBatisPlusBootStarterTest = "${Group.mabatisPlus}:mybatis-plus-boot-starter-test:${Version.mybatisPlus}"

    val guava = "com.google.guava:guava:31.1-jre"

    val commonsLang = "${Group.apacheCommons}:commons-lang3:3.12.0"
    val commonsPool = "${Group.apacheCommons}:commons-pool2:2.11.1"

    val lombok = "org.projectlombok:lombok:1.18.24"
    val lombokMapstruct = "org.projectlombok:lombok-mapstruct-binding:0.2.0"
    val mapstruct = "org.mapstruct:mapstruct:1.5.2.Final"
    val mapstructProcessor = "org.mapstruct:mapstruct-processor:1.5.2.Final"

    val mysqlConnectorJava = "mysql:mysql-connector-java:8.0.27"

    val grpcmockSpringBoot = "org.grpcmock:grpcmock-spring-boot:0.7.6"

    val embeddedRedis = "it.ozimov:embedded-redis:0.7.2"

    val jsonUnitFluent = "net.javacrumbs.json-unit:json-unit-fluent:2.34.0"

    val junit = "junit:junit:4.13"
    val junitVintage = "org.junit.vintage:junit-vintage-engine:5.6.2"
    val junitJupiterEngine = "org.junit.jupiter:junit-jupiter-engine:5.8.2"

    val testng = "org.testng:testng:6.14.3"

    val fluentHttpApi = "org.apache.httpcomponents:fluent-hc:4.5.1"

    val springKafkaApi = "org.springframework.kafka:spring-kafka:2.8.1"

    val slf4jApi = "org.slf4j:slf4j-api:1.7.25"
    val logbackCoreApi = "ch.qos.logback:logback-core:1.1.11"
    val logbackClassicApi = "ch.qos.logback:logback-classic:1.1.11"

    var vertxCore = "io.vertx:vertx-core:${Version.vertxVersion}"
    var vertxWeb = "io.vertx:vertx-web:${Version.vertxVersion}"
    var vertxGrpcProtoc = "io.vertx:vertx-grpc-protoc-plugin2:4.5.1"
    var vertxGrpcServer = "io.vertx:vertx-grpc-server:${Version.vertxVersion}"
    var vertxGrpcClient = "io.vertx:vertx-grpc-client:${Version.vertxVersion}"

    var jacksonDatabind = "com.fasterxml.jackson.core:jackson-databind:${Version.jacksonVersion}"
    var jacksonCore = "com.fasterxml.jackson.core:jackson-core:${Version.jacksonVersion}"
    var jacksonAnnotations = "com.fasterxml.jackson.core:jackson-annotations:${Version.jacksonVersion}"

    var okhttp = "com.squareup.okhttp3:okhttp:4.12.0"

    var akkaActor = "com.typesafe.akka:akka-actor_2.12:${Version.akkaVersion}"

    val mockito = "org.mockito:mockito-inline:3.12.4"

    val grpcClientAutoConfigure = "net.devh:grpc-client-spring-boot-autoconfigure:2.13.1.RELEASE"
    val grpcClientStarter = "net.devh:grpc-client-spring-boot-starter:2.13.1.RELEASE"
}

@file:Suppress("UnstableApiUsage")

rootProject.name = "xspring"

// include part
// See https://github.com/gradle/gradle/issues/11090 for more details.
rootDir
    .walk()
    .maxDepth(1)
    .filter {
        it.name != "buildSrc"
                && it.isDirectory
                && file("${it.absolutePath}/build.gradle.kts").exists()
    }
    .forEach {
        include(":${it.name}")
    }
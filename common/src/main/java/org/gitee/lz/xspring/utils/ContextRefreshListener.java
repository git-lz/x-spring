package org.gitee.lz.xspring.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Service;

/**.
 * @ClassName: ContextRefreshListener
 * @Description: ContextRefreshListener
 * @Author: lizhen
 * @Date: 2024-01-03 11:29
 * @Version: 1.0
 **/
@Service
@Slf4j
public class ContextRefreshListener implements ApplicationListener<ContextRefreshedEvent> {

  /**
   * Handle an application event.
   *
   * @param event the event to respond to
   */
  @Override
  public void onApplicationEvent(ContextRefreshedEvent event) {
    // TODO Auto-generated method stub
    if (event.getApplicationContext().getDisplayName().equals("Root WebApplicationContext")) {
      System.out.println("避免spring-servlet.xml跟appicationContext.xml两次加载");
    }

    if (event.getApplicationContext().getParent() == null) {
      System.out.println("执行context上下文初始化");
      SpringContextUtil.instance.init(event.getApplicationContext());
    }
  }
}

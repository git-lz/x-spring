package org.gitee.lz.xspring.exception;

/**
 * @ClassName: Assert
 * @Description: assert
 * @Author: lizhen
 * @Date: 2024-01-09 15:33
 * @Version: 1.0
 **/
public interface Assert {
  /**.
   * 创建异常
   * @param args
   * @return
   */
  BaseException newException(Object... args);

  /**.
   * 创建异常
   * @param t not null
   * @param args nullable
   * @return BaseException
   */
  BaseException newException(Throwable t, Object... args);

  /**.
   * 断言对象obj非空。如果对象obj为空，则抛出异常
   * @param obj 待判断对象
   */
  default void assertNotNull(Object obj) {
    if (obj == null) {
      throw newException(obj);
    }
  }

  /**.
   * 断言对象obj非空。如果对象obj为空，则抛出异常。
   * 异常信息message支持传递参数方式，避免在判断之前进行字符串拼接操作
   * @param obj 待判断对象
   * @param args message占位符对应的参数列表
   */
  default void assertNotNull(Object obj, Object... args) {
    if (obj == null) {
      throw newException(args);
    }
  }

  /**.
   * 断言sql是否执行成功
   * @param obj not null
   */
  default void assertSqlSuccess(int obj) {
    if (obj == 0) {
      throw newException(obj);
    }
  }
}


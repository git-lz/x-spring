package org.gitee.lz.xspring.dto.pojo.common;

import com.google.common.base.MoreObjects;
import com.tigerbrokers.bff.apihub.common.dto.pojo.v1.response.ExecResult;
import com.tigerbrokers.bff.apihub.common.exception.BusinessExceptionAssert;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @ClassName: ResponseEnum
 * @Description: ResponseEnum
 * @Author: lizhen
 * @Date: 2024-01-09 16:04
 * @Version: 1.0
 **/
@Getter
@AllArgsConstructor
public enum ResponseEnum implements BusinessExceptionAssert {
  // MYSQL EXCEPTION
  MYSQL_INSERT_FAILED(ExecResult.CODE_SQL_EXCEPTION, "sql error"),

  // Task Not Found
  TASK_NOT_FOUND(ExecResult.CODE_TASK_NOT_FOUND, "task not found"),
  ;

  // 返回码
  private int ret;

   // 返回消息
  private String message;

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
      .add("ret", ret)
      .add("message", message)
      .toString();
  }
}

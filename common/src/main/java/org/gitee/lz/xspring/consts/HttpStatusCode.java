package org.gitee.lz.xspring.consts;

/**.
 * @ClassName: HttpStatusCode
 * @Description: http响应状态码
 * @Author: lizhen
 * @Date: 2023-12-22 12:07
 * @Version: 1.0
 **/
public class HttpStatusCode {
  public static int ok = 200;
  public static int created = 201;
  public static int badRequest = 400;
  public static int unauthorized = 401;
  public static int forbidden = 403;
  public static int notFound = 404;
  public static int internalErr = 500;
}

package org.gitee.lz.xspring.dto.pojo.common;

/**
 * @ClassName: IResponseEnum
 * @Description: IResponseEnum
 * @Author: lizhen
 * @Date: 2024-01-09 15:45
 * @Version: 1.0
 **/
public interface IResponseEnum {
  int getRet();

  String getMessage();
}

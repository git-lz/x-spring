package org.gitee.lz.xspring.utils;

/**.
 * @ClassName: StringUtils
 * @Description: 字符串工具类
 * @Author: lizhen
 * @Date: 2023-12-22 15:24
 * @Version: 1.0
 **/
public class StringUtils {

  /**.
   * @param originalString not null
   * @param prefix not null
   * @return 去除前缀后的字符串
   * @throws RuntimeException 如果没有该前缀，则抛出异常
   */
  public static String trimPrefix(String originalString, String prefix) throws RuntimeException {
    if (originalString.startsWith(prefix)) {
      return originalString.substring(prefix.length());
    }

    throw new RuntimeException("String doesn't start with the specified prefix.");
  }
}

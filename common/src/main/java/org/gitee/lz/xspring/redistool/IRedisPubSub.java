package org.gitee.lz.xspring.redistool;


import org.gitee.lz.xspring.consts.CacheKeyEnum;

/**.
 * @ClassName: IRedisPubSub
 * @Description: IRedisPubSub 实现redis发布订阅功能
 * @Author: lizhen
 * @Date: 2024-01-05 19:25
 * @Version: 1.0
 **/
public interface IRedisPubSub {
  /**.
   * receiveMessage 接收消息
   * @param message not null
   */
  void receiveMessage(String message);

  /**.
   * 发布订阅监听的key
   * @return
   */
  CacheKeyEnum getCacheKeyEnum();
}

package org.gitee.lz.xspring.exception;

import java.text.MessageFormat;

import com.tigerbrokers.bff.apihub.common.dto.pojo.common.IResponseEnum;

/**
 * @ClassName: BusinessExceptionAssert
 * @Description: BusinessExceptionAssert
 * @Author: lizhen
 * @Date: 2024-01-09 16:03
 * @Version: 1.0
 **/
public interface BusinessExceptionAssert extends IResponseEnum, Assert {
  @Override
  default BaseException newException(Object... args) {
    String msg = MessageFormat.format(this.getMessage(), args);
    return new BusinessException(this, args, msg);
  }

  @Override
  default BaseException newException(Throwable t, Object... args) {
    String msg = MessageFormat.format(this.getMessage(), args);
    return new BusinessException(this, args, msg, t);
  }
}

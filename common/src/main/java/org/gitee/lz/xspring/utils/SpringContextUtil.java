package org.gitee.lz.xspring.utils;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**.
 * @ClassName: SpringContextUtils
 * @Description: spring context工具类
 * @Author: lizhen
 * @Date: 2024-01-02 23:13
 * @Version: 1.0
 **/
@Component
public class SpringContextUtil {

  public static SpringContextUtil instance = new SpringContextUtil();

  private ApplicationContext applicationContext;

  private SpringContextUtil() {

  }

  public void init(ApplicationContext context) {
    applicationContext = context;
  }

  public ApplicationContext getContext() {
    return applicationContext;
  }

  /**.
   * 获取Bean
   * @param requiredType not null
   * @return T nullable
   */
  public <T> T getBean(Class<T> requiredType) {
    return applicationContext.getBean(requiredType);
  }
}
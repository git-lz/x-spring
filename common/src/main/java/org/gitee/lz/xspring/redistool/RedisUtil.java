package org.gitee.lz.xspring.redistool;

import com.tigerbrokers.bff.apihub.common.consts.CacheKeyEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**.
 * @ClassName: RedisUtil
 * @Description: redis工具类
 * @Author: lizhen
 * @Date: 2024-01-05 19:38
 * @Version: 1.0
 **/
@Slf4j
@Component
@Service
public class RedisUtil {

  @Autowired
  private StringRedisTemplate redisStringTmpl;

  /**.
   * 消息发布
   * @param cacheKeyEnum not null
   * @param message not null
   */
  public void publish(CacheKeyEnum cacheKeyEnum, Object message){
    redisStringTmpl.convertAndSend(cacheKeyEnum.getKey() , message);
  }

  /**.
   * 反序列化redis数据
   * @param value not null
   * @param <T> not null
   * @return <T> not null
   */
  public <T> T deserialize(String value){
    final RedisSerializer<?> valueSerializer = redisStringTmpl.getValueSerializer();
    final Object deserialize = valueSerializer.deserialize(value.getBytes());
    return deserialize == null ? null : (T) deserialize;
  }
}


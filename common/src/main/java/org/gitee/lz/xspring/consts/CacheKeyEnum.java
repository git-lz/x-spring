package org.gitee.lz.xspring.consts;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @ClassName: CacheKeyEnum
 * @Description: CacheKeyEnum
 * @Author: lizhen
 * @Date: 2024-01-05 19:26
 * @Version: 1.0
 **/
@AllArgsConstructor
@Getter
public enum CacheKeyEnum {

  PUBSUB_TASK_CONFIG_QUEUE("apihub-pubsub:task-config", 0L);

  // 缓存键名
  private String key;

  /**
   * 过期时间，单位秒
   * 0 表示不过期
   */
  private Long expireTime;

}

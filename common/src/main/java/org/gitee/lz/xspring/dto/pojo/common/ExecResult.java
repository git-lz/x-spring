package org.gitee.lz.xspring.dto.pojo.common;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Nonnull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ExecResult {
  private int ret;

  private long serverTime;

  private String message;

  private Object data;

  public static final int CODE_SUCCESS = 0;

  // 内部错误
  public static final int CODE_UNKNOWN_ERROR = -1;
  public static final int CODE_RUNTIME_EXCEPTION = 10;
  public static final int CODE_SQL_EXCEPTION = 11;
  public static final int CODE_INVALID_PARAM = 12;
  public static final int CODE_ILLEGAL_ARGS = 13;
  public static final int CODE_PARSE_EXCEL_FAIL = 14;
  public static final int CODE_BAD_REQUEST = 15;
  public static final int CODE_UNAUTHORIZED = 16;
  public static final int CODE_CONDITION_NOT_PASS = 17;
  public static final int CODE_TASK_NOT_FOUND = 18;

  public static final String MSG_INVALID_PARAM = "invalid param";
  public static final String MSG_UNKNOWN = "unknown";
  public static final String MSG_FAIL_INSERT = "insert data failure";
  public static final String MSG_FAIL_UPDATE = "update data failure";
  public static final String MSG_FAIL_INSERT_OR_UPDATE = "insert or update data failure";
  public static final String MSG_FAIL_DELETE = "delete data failure";
  public static final String MSG_RESOLVE_EXCEL_FAIL = "parse excel fails";
  public static final String MSG_HTTP_CLIENT_FAIL = "http client failed";
  public static final String MSG_CONDITION_NOT_PASS = "condition not pass";
  public static final String MSG_AMS_GET_PROFILE_FAILED = "get profile failed";
  public static final String MSG_BFF_GET_MODULE_INFO_FAILED = "get module info failed";

  // 下游错误码从1000开始定义，每种业务类型的错误码按1000个分配，1000~1999
  public static final int CODE_AMS_GET_PROFILE_FAILED = 1001;

  public static final int CODE_BFF_GET_MODULE_INFO_FAILED = 2001;

  private static final Map<Integer, String> CODE_2_MSG_MAP = new HashMap<Integer, String>() {
    {
      put(CODE_SUCCESS, "SUCCESS");
      put(CODE_CONDITION_NOT_PASS, MSG_CONDITION_NOT_PASS);
      put(CODE_AMS_GET_PROFILE_FAILED, MSG_AMS_GET_PROFILE_FAILED);
      put(CODE_BFF_GET_MODULE_INFO_FAILED, MSG_BFF_GET_MODULE_INFO_FAILED);
    }
  };

  public static ExecResult ofFail(String message) {
    long timestamp = System.currentTimeMillis();
    return new ExecResult(CODE_UNKNOWN_ERROR, timestamp, message, null);
  }

  @JsonIgnore
  public boolean isSuccess() {
    return this.ret == 0;
  }

  /**.
   * 生成返回结果。
   * @param isSuccess not null
   * @param data not null
   * @return
   */
  @Nonnull
  public static ExecResult of(boolean isSuccess, Object data) {
    long timestamp = System.currentTimeMillis();
    return isSuccess
      ? new ExecResult(0, timestamp, "", data) :
      new ExecResult(CODE_UNKNOWN_ERROR, timestamp, MSG_UNKNOWN, data);
  }

  /**.
   * 生成返回结果
   * @param isSuccess not null
   * @param msg not null
   * @param data not null
   * @return
   */
  @Nonnull
  public static ExecResult of(boolean isSuccess, String msg, Object data) {
    long timestamp = System.currentTimeMillis();
    return isSuccess ? new ExecResult(0, timestamp, msg, data) :
      new ExecResult(CODE_UNKNOWN_ERROR, timestamp, msg, data);
  }

  /**
   * .
   * 生成带msg信息的Execute result
   *
   * @param code not null
   * @param data nullable
   * @return ExecResult
   */
  @Nonnull
  public static ExecResult of(int code, Object data) {
    long timestamp = System.currentTimeMillis();

    String msg = "";
    if (!CODE_2_MSG_MAP.get(code).equals("")) {
      msg = CODE_2_MSG_MAP.get(code);
    }

    return new ExecResult(code, timestamp, msg, data);
  }

  /**
   * .
   * 生成带msg信息的Execute result
   *
   * @param code not null
   * @return ExecResult
   */
  @Nonnull
  public static ExecResult of(int code) {
    long timestamp = System.currentTimeMillis();
    return new ExecResult(code, timestamp, "", null);
  }

  /**.
   * 生成返回结果
   * @param code not null
   * @param message not null
   */
  public static ExecResult of(int code, String message) {
    long timestamp = System.currentTimeMillis();
    return new ExecResult(code, timestamp, message, "");
  }

}

package org.gitee.lz.xspring.utils;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

/**
 * .
 *
 * @ClassName: JacksonUtils
 * @Description: jackson 工具类
 * @Author: lizhen
 * @Date: 2023-12-25 18:55
 * @Version: 1.0
 **/
@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class JacksonUtils {

}

package org.gitee.lz.xspring.exception;

import java.io.Serializable;
import java.text.MessageFormat;

import com.sun.istack.internal.NotNull;
import lombok.Getter;
import org.gitee.lz.xspring.dto.pojo.common.IResponseEnum;

/**
 * @ClassName: BaseException
 * @Description: BaseException
 * @Author: lizhen
 * @Date: 2024-01-09 15:33
 * @Version: 1.0
 **/
@Getter
public class BaseException extends RuntimeException implements Serializable {
  IResponseEnum responseEnum;

  public BaseException() {
  }

  public BaseException(String message) {
    super(message);
  }

  public BaseException(Throwable cause) {
    super(cause);
  }

  public BaseException(String message, Throwable cause) {
    super(message, cause);
  }

  public BaseException(@NotNull IResponseEnum responseEnum, Object[] args, String message) {
    super(MessageFormat.format("response={0}, " + message, responseEnum.getMessage(), args));
    this.responseEnum = responseEnum;
  }

  public BaseException(@NotNull IResponseEnum responseEnum, Object[] args, String message, Throwable cause) {
    super(MessageFormat.format("response={0}, " + message, responseEnum.getMessage(), args));
    this.responseEnum = responseEnum;
  }
}

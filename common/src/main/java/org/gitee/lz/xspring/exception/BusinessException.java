package org.gitee.lz.xspring.exception;

import org.gitee.lz.xspring.dto.pojo.common.IResponseEnum;

/**
 * @ClassName: BusinessException
 * @Description: BusinessException, 业务处理时，出现异常，可以抛出该异常
 * @Author: lizhen
 * @Date: 2024-01-09 15:46
 * @Version: 1.0
 **/
public class BusinessException extends BaseException {
  private static final long serialVersionUID = 1L;

  public BusinessException(IResponseEnum responseEnum, Object[] args, String message) {
    super(responseEnum, args, message);
  }

  public BusinessException(IResponseEnum responseEnum, Object[] args, String message, Throwable cause) {
    super(responseEnum, args, message, cause);
  }
}

plugins {
    id("java")
    id("net.linguica.maven-settings") version "0.5"
    id("com.gitee.lz.libs")
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(libs.lombok)
    implementation(libs.springBootConfigProcessor)
    implementation(libs.springActuatorAutoConfig)

    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.1")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.8.1")
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}